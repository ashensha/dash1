variable "ssh_public_key_file" {
  type        = string
  description = "Path to the public key file to be used for deployment to EC2"
}
