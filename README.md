# dash1

This project provides the definition of (virtual) infrastructure to run [**zero**](https://gitlab.com/peter.saarland/zero) as *infrastructure as code* (IaC). The primary tool for this is [terraform](https://www.terraform.io/).

## Terraform modules

- [AWS](./modules/aws/)
- [Digitalocean](./modules/digitalocean/)
- [IBM Cloud](./modules/ibm/)

## State backends

- [S3](./backends/s3/)

## Examples

Examples that can be reused by projects using **dash1** and **zero** as a starting point

- [AWS with S3 backend](./examples/aws/)

## Local testing

Run `make ssh-gen` to generate a local SSH key pair and use the [Makefile](./Makefile) targets to setup/teardown the infrastructure.

⚠️ The `Makefile` does not provide targets for the example and backends.




