
# Define Hetzner provider
provider "hcloud" {
  token = var.ACCESS_TOKEN
}

#create fingerprint
resource "hcloud_ssh_key" "ssh_key" {
  name = var.ssh_key_name
  public_key = file(var.ssh_public_key_file)
}

# Create a manager server
resource "hcloud_server" "manager" {
  count = var.manager_instances
  name = var.manager_instances > 1 ? join("-",["Manager",count.index]) : "Manager"
  image = var.server_type == "ubuntu" ? "ubuntu-18.04" : "debian-9"
  server_type = var.server_size
  ssh_keys  = [hcloud_ssh_key.ssh_key.id]
  labels = var.labels
}

# Create a worker server
resource "hcloud_server" "worker" {
  count = var.worker_instances
  name = var.worker_instances > 1 ? join("-",["worker",count.index]) : "worker"
  image = var.server_type == "ubuntu" ? "ubuntu-18.04" : "debian-9"
  server_type = var.server_size
  ssh_keys  = [hcloud_ssh_key.ssh_key.id]
  labels = var.labels
}

#  create volume manager
resource "hcloud_volume" "manager" {
  count = var.volume_count * var.manager_instances
  name = "zero-volume-manager-${count.index}"
  location = element(hcloud_server.manager.*.location, floor(count.index / var.volume_count))
  size     = var.volume_size
}

#  create volume worker
resource "hcloud_volume" "worker" {
  count = var.volume_count * var.worker_instances
  name = "zero-volume-worker-${count.index}"
  location = element(hcloud_server.worker.*.location, floor(count.index / var.volume_count))
  size     = var.volume_size
}

#  attach volume manager
resource "hcloud_volume_attachment" "manager" {
  count = var.manager_instances*var.volume_count
  volume_id = hcloud_volume.manager[count.index].id
  server_id = element(hcloud_server.manager.*.id, floor(count.index / var.volume_count))
  automount = true
}

#  attach volume worker
resource "hcloud_volume_attachment" "worker" {
  count = var.worker_instances*var.volume_count
  volume_id = hcloud_volume.worker[count.index].id
  server_id = element(hcloud_server.worker.*.id, floor(count.index / var.volume_count))
  automount = true
}

#create network in hcloud
resource "hcloud_network" "privNet" {
  name = var.network_name
  ip_range = var.ip_range
}

#config location/type and ip range to network
resource "hcloud_network_subnet" "foonet" {
  network_id = hcloud_network.privNet.id
  type = "server"
  network_zone = var.network_zone
  ip_range   = var.ip_range
}

#attach the network to manager instance
resource "hcloud_server_network" "Manager-network" {
  count = var.manager_instances
  server_id = hcloud_server.manager[count.index].id
  network_id = hcloud_network.privNet.id
}

#attach the network to worker instance
resource "hcloud_server_network" "Worker-network" {
  count = var.worker_instances
  server_id = hcloud_server.worker[count.index].id
  network_id = hcloud_network.privNet.id
}
