variable "ACCESS_TOKEN" {
    description = "hcloud token taken from the HETZNER cloud."
}

variable "ssh_public_key_file" {
  type        = string
  description = "The Path to the local SSH public key file to be used to connect to the instance"
}

variable "ssh_key_name" {
  type        = string
  description = "Name of the ssh-key"
  default = "Zero-SSH-key"
}

variable "manager_instances" {
    description = "No of manager server count"
    default = 1
}

variable "worker_instances" {
    description = "No of worker server count"
    default = 0
}

variable "volume_count" {
    description = "No of Volumes in a instance"
    default = 0
}

variable "volume_size" {
    type = number
    description = "Size of a Volume in an instance"
    default = 20
}

variable "server_type" {
    description = "server OS of the VM. valid values are ubuntu and debian"
}

variable "server_size" {
    description = "Size of the server"
}

variable "network_name" {
    description = "network name"
    default = "my-network"
}

variable "network_zone" {
    description = "network zone the network belongs to"
    default = "eu-central"
}

variable "ip_range" {
    description = "IP Range of the whole Network which must span all included subnets and route destinations. Must be one of the private ipv4 ranges of RFC1918"
    default = "10.16.0.0/16"
}

variable "labels" {
  description = "User-defined labels "
  type        = map
  default = {
            "deployement" = "Terraform"
            }
}
