**dash1 for Hetzner Cloud**
Terraform module to setup dash1 as infrastructure for zero on hetzner cloud. Key facts are:

## **Deploys N number of Debian/CentOS Virtual Machines to Hetzner Cloud** 

- Ability to specify and deploy manager and worker instances
- support for single manager cluster
- Ability to add labels
- Ability to specify server size
- Ability to specify no of volumes
- Ability to specify a IP range for the private ip address



> Note: For module to work it needs number of required variables corresponding to an existing resources in vSphere. Please refer to variables.tf for the list of required variables.

Defaults in variables.tf

```
ssh_key_name = "Zero-SSH-key"
manager_instances = 1
worker_instances = 0
volume_count = 0
volume_size = 20
network_name = "my-network"
network_zone = "eu-central"
ip_range = "10.16.0.0/16"
labels = {
            "deployement" = "Terraform"
         }
```




Usage

```
hcloud_token= "<Insert token here>"
finger= "<insert fingerprint here>"
server_type = "debian" #possible values are ubuntu and debian
volume_count =  #insert number of volumes here
server_size = "server size"
ip_range = "" #format 10.0.1.0/24
labels = {
           "Environment" = ""
           "Location" = ""
           "Technology" = ""
           "deployement" = ""
           }
```
