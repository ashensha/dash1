# Output manager
output "ZERO_NODES_MANAGER" {
 value = "${join(",",hcloud_server.manager.*.ipv4_address)}"
}

output "ZERO_NODES_WORKER" {
 value = "${join(",",hcloud_server.worker.*.ipv4_address)}"
}

output "ZERO_PROVIDER" {
 value = "hcloud"
}

output "ZERO_CLUSTER_NETWORK" {
 value = hcloud_network.privNet.ip_range
}

output "ZERO_INGRESS_IP" {
 value = hcloud_server.manager[0].ipv4_address 
}