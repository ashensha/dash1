output "manager_ip_address" {
  value = digitalocean_droplet.manager.*.ipv4_address
}

output "worker_ip_address" {
  value = digitalocean_droplet.worker.*.ipv4_address
}

output "satellite_ip_address" {
  value = digitalocean_droplet.satellite.*.ipv4_address
}
