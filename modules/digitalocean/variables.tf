# Defaults provided

variable "region" {
  type        = string
  description = "Region to deploy all resources to"
  default     = "fra1"
}

variable "environment" {
  type        = string
  description = "The name to be used within all ressources as an environment identifier"
  default     = "staging"
}

variable "manager_count" {
  type        = number
  description = "Number of manager instances in the zero cluster"
  default     = 1
}

variable "worker_count" {
  type        = number
  description = "Number of worker instances in the zero cluster"
  default     = 1
}

variable "satellite_count" {
  type        = number
  description = "Number of satellite instances in the zero cluster"
  default     = 1
}

variable "volume_count" {
  type        = number
  description = "Number of storage volumes in the zero cluster"
  default     = 1
}

variable "droplet_image" {
  type        = string
  description = "Image identifier of the OS image to be used"
  default     = "ubuntu-18-04-x64"
}

variable "droplet_size" {
  type        = string
  description = "Computing size identifier to be used for the droplet"
  default     = "s-2vcpu-4gb"
}

variable "volume_size" {
  type        = number
  description = "The number of GBs assigned to each volume"
  default     = 50
}

variable "tags" {
  type        = set(string)
  description = "Set of tags applied to all ressources managed by this module"
  default     = []
}

# NO defaults provided

variable "access_token" {
  type        = string
  description = "The Digitalocean API access token to be used for provisioning"
}

variable "ssh_public_key_file" {
  type        = string
  description = "The Path to the local SSH private key file to be used to connect to the instance"
}
