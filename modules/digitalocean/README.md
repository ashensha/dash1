# dash1 for Digitalocean

Terraform module to setup **dash1** as infrastructure for **zero** on **Digitalocean**. Key facts are:

- All managers, workers and satellites are deployed as droplets
- Volumes are used as storages and attached to all droplets
- `access_token` and `ssh_public_key_file` are required variables, all others provide defaults, see [variables.tf](./variables.tf)
- Public IPs of all nodes are provided as outputs