provider "digitalocean" {
  token = var.access_token
}

resource "digitalocean_tag" "app" {
  name = "zero"
}

resource "digitalocean_tag" "environment" {
  name = var.environment
}

locals {
  # Common tags to be assigned to all resources
  common_tags = [digitalocean_tag.app.id, digitalocean_tag.environment.id]

  all_tags = merge(local.common_tags, var.tags)
}

resource "digitalocean_ssh_key" "ssh_key" {
  name       = "zero-${var.environment}-ssh-key"
  public_key = file(var.ssh_public_key_file)
}

resource "digitalocean_volume" "manager" {
  region      = var.region
  count       = var.volume_count * var.manager_count
  name        = "zero-${var.environment}-manager-vol-${count.index}"
  size        = var.volume_size
  description = "zero-${var.environment}-manager-vol-${count.index}"
  tags        = local.all_tags
}

resource "digitalocean_volume" "worker" {
  region      = var.region
  count       = var.volume_count * var.worker_count
  name        = "zero-${var.environment}-worker-vol-${count.index}"
  size        = var.volume_size
  description = "zero-${var.environment}-worker-vol-${count.index}"
  tags        = local.all_tags
}

resource "digitalocean_droplet" "manager" {
  count              = var.manager_count
  name               = "zero-${var.environment}-manager-${count.index + 1}"
  image              = var.droplet_image
  region             = var.region
  size               = var.droplet_size
  ssh_keys           = [digitalocean_ssh_key.ssh_key.fingerprint]
  private_networking = true
  backups            = false
  monitoring         = true
  ipv6               = true
  tags               = local.all_tags
}

resource "digitalocean_volume_attachment" "manager" {
  count      = var.manager_count * var.volume_count
  droplet_id = element(digitalocean_droplet.manager.*.id, floor(count.index % var.manager_count))
  volume_id  = element(digitalocean_volume.manager.*.id, count.index)
}

resource "digitalocean_droplet" "worker" {
  count              = var.worker_count
  name               = "zero-${var.environment}-worker-${count.index + 1}"
  image              = var.droplet_image
  region             = var.region
  size               = var.droplet_size
  ssh_keys           = [digitalocean_ssh_key.ssh_key.fingerprint]
  private_networking = true
  backups            = false
  monitoring         = true
  ipv6               = true
  tags               = local.all_tags
}

resource "digitalocean_volume_attachment" "worker" {
  count      = var.worker_count * var.volume_count
  droplet_id = element(digitalocean_droplet.worker.*.id, floor(count.index / var.volume_count))
  volume_id  = element(digitalocean_volume.worker.*.id, count.index)
}

resource "digitalocean_droplet" "satellite" {
  count              = var.worker_count
  name               = "zero-${var.environment}-satellite-${count.index + 1}"
  image              = var.droplet_image
  region             = var.region
  size               = var.droplet_size
  ssh_keys           = [digitalocean_ssh_key.ssh_key.fingerprint]
  private_networking = true
  backups            = false
  monitoring         = true
  ipv6               = true
  tags               = local.all_tags
}
