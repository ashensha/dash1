.PHONY: ssh-gen digitalocean-init digitalocean-validate digitalocean-plan ibm-init ibm-validate ibm-plan ibm-apply ibm-destroy vsphere-init vsphere-validate vsphere-plan vsphere-apply vsphere-destroy hcloud-init hcloud-validate hcloud-plan hcloud-apply hcloud-destroy

# Configuration of terraform variables works through TF_VAR_<exported variable>
include .env

ssh_gen:
	ssh-keygen -t rsa -b 4096 -f ./${SSH_KEY_FILE} -N ''

# Digitalocean

digitalocean-init:
	@cd modules/digitalocean \
		&& terraform init

digitalocean-validate:
	@cd modules/digitalocean \
		&& terraform validate

digitalocean-plan:
	@cd modules/digitalocean \
		&& export TF_VAR_access_token=${ACCESS_TOKEN} \
		&& export TF_VAR_ssh_public_key_file=${SSH_PUBLIC_KEY_FILE} \
		&& terraform plan -out "digitalocean-planfile"


# vsphere

vsphere-init:
	@cd modules/vsphere \
		&& terraform init

vsphere-validate:
	@cd modules/vsphere \
		&& terraform validate

vsphere-plan:
	@cd modules/vsphere \
		&& terraform plan -out "vsphere-planfile"

vsphere-apply:
	@cd modules/vsphere \
		&& terraform apply -auto-approve

vsphere-destroy:
	@cd modules/vsphere \
		&& terraform destroy -auto-approve

# hcloud

hcloud-init:
	@cd modules/hcloud \
		&& terraform init

hcloud-validate:
	@cd modules/hcloud \
		&& terraform validate

hcloud-plan:
	@cd modules/hcloud \
		&& export TF_VAR_access_token=${ACCESS_TOKEN} \
		&& export TF_VAR_ssh_public_key_file=${SSH_PUBLIC_KEY_FILE} \
		&& terraform plan -out "hcloud-planfile"

hcloud-apply:
	@cd modules/hcloud \
		&& export TF_VAR_access_token=${ACCESS_TOKEN} \
		&& export TF_VAR_ssh_public_key_file=${SSH_PUBLIC_KEY_FILE} \
		&& terraform apply -auto-approve

hcloud-destroy:
	@cd modules/hcloud \
		&& export TF_VAR_access_token=${ACCESS_TOKEN} \
		&& export TF_VAR_ssh_public_key_file=${SSH_PUBLIC_KEY_FILE} \
		&& terraform destroy -auto-approve

# IBM

ibm-init:
	@cd modules/ibm \
		&& terraform init

ibm-validate:
	@cd modules/ibm \
		&& terraform validate

ibm-plan:
	@cd modules/ibm \
		&& export TF_VAR_access_token=${ACCESS_TOKEN} \
		&& export TF_VAR_ssh_public_key_file=${SSH_PUBLIC_KEY_FILE} \
		&& export TF_VAR_resource_group_name=${IBM_RESOURCE_GROUP_NAME} \
		&& terraform plan -out "ibm-planfile"

ibm-apply:
	@cd modules/ibm \
		&& export TF_VAR_access_token=${ACCESS_TOKEN} \
		&& export TF_VAR_ssh_public_key_file=${SSH_PUBLIC_KEY_FILE} \
		&& export TF_VAR_resource_group_name=${IBM_RESOURCE_GROUP_NAME} \
		&& terraform apply -auto-approve

ibm-destroy:
	@cd modules/ibm \
		&& export TF_VAR_access_token=${ACCESS_TOKEN} \
		&& export TF_VAR_ssh_public_key_file=${SSH_PUBLIC_KEY_FILE} \
		&& export TF_VAR_resource_group_name=${IBM_RESOURCE_GROUP_NAME} \
		&& terraform destroy -auto-approve

# AWS

aws-init:
	@cd modules/aws \
		&& terraform init

aws-validate:
	@cd modules/aws \
		&& terraform validate

aws-plan:
	@cd modules/aws \
		&& export TF_VAR_access_token=${ACCESS_TOKEN} \
		&& export TF_VAR_ssh_public_key_file=${SSH_PUBLIC_KEY_FILE} \
		&& terraform plan -out "aws-planfile"

aws-apply:
	@cd modules/aws \
		&& export TF_VAR_access_token=${ACCESS_TOKEN} \
		&& export TF_VAR_ssh_public_key_file=${SSH_PUBLIC_KEY_FILE} \
		&& terraform apply -auto-approve

aws-destroy:
	@cd modules/aws \
		&& export TF_VAR_access_token=${ACCESS_TOKEN} \
		&& export TF_VAR_ssh_public_key_file=${SSH_PUBLIC_KEY_FILE} \
		&& terraform destroy -auto-approve